package Debuggerpablo;

import java.util.Scanner;

public class debuggerpablo {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		boolean hayMultiplos = false;

		for (int i = 0; i < 5; i++) {
			System.out.println("Dame un numero");
			int numero = input.nextInt();

			if (numero * 3 == 0) {
				hayMultiplos = true;
			}
		}

		if (!hayMultiplos) {
			System.out.println("Hay multiplos");
		} else {
			System.out.println("No hay multiplos");
		}

		input.close();
	}

}